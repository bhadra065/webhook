from flask import Flask, request, json
from codes.datastore import Db
app = Flask(__name__)
datastore = Db()


@app.route('/post', methods=['POST'])
def process_request():
    data = request.json
    datastore.save_request(data[0], data[1], data[2], data[5], data[3], data[4])
    return "Received hook"


if __name__ == '__main__':
    app.run(debug=True, port=5001)
