import MySQLdb


class Db:
    def __init__(self):
        self.db = MySQLdb.connect("localhost", "root", "", "webhook")
        self.cursor = self.db.cursor()

    def save_request(self, puzzle_string, start, result_string, end, total_time, request_id):
        self.cursor.execute('INSERT INTO requests (id, input_string, output_string, start_time, end_time, total_time) VALUES (%s, %s, %s, %s, %s, %s)', [request_id, puzzle_string, result_string, end, total_time,start])
        self.db.commit()
        return self.cursor.lastrowid